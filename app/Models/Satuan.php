<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Satuan extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'satuan';

    protected $fillable = [
        'nama',
        'kode',
        'rasio',
        'created_at',
        'updated_at'
    ];
}
