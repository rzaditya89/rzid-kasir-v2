<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'kategori';
    protected $fillable = [
        'name',
        'note',
        'created_by',
        'created_at',
        'updated_at'
    ];

    public function CreateBy()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
    }
}
