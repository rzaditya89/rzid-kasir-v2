<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $fillable = [
        'skey',
        'name',
        'address',
        'contact',
        'email',
        'note',
        'created_at',
        'updated_at'
    ];
}
