<?php

namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use App\Models\Supplier;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;

class SuppliersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suppliers = Supplier::paginate(7);
        return view('pages.suppliers.index', compact('suppliers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate data
        $request->validate(
            [
                'name' => 'required|max:255',
                'email' => 'required',
                'address' => 'required',
                'contact' => 'required',
            ],
            [
                //  Error message 
                'name.required' => 'Nama Supplier harus diisi',
                'email.required' => 'Email harus diisi',
                'address.required' => 'Alamat harus diisi',
                'contact.required' => 'Kontak harus diisi',
            ]
        );

        Supplier::create([
            'skey' => Uuid::uuid4(),
            'name' => $request->name,
            'address' => $request->address,
            'contact' => $request->contact,
            'email' => $request->email,
            'note' => $request->note,
            'created_at' => Carbon::now('Asia/Jakarta'),
        ]);

        return redirect('suppliers')->with('status', 'Data Berhasil ditambah!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function show(Supplier $supplier)
    {
        // dd($supplier);
        return view('pages.suppliers.show', compact('supplier'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function edit(Supplier $supplier)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Supplier $supplier)
    {
        // Validate data
        $request->validate(
            [
                'name' => 'required|max:255',
                'email' => 'required',
                'address' => 'required',
                'contact' => 'required',
            ],
            [
                //  Error message 
                'name.required' => 'Nama Supplier tidak boleh kosong',
                'email.required' => 'Email tidak boleh kosong',
                'address.required' => 'Alamat tidak boleh kosong',
                'contact.required' => 'Kontak tidak boleh kosong',
            ]
        );

        Supplier::where('skey', $supplier->skey)
            ->update([
                'name' => $request->name,
                'address' => $request->address,
                'email' => $request->email,
                'contact' => $request->contact,
                'note' => $request->note,
                'updated_at' => Carbon::now('Asia/Jakarta'),
            ]);

        return redirect('suppliers')->with('status', 'Data Berhasil diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function destroy(Supplier $supplier)
    {
        //
    }
}
