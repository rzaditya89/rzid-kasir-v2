<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');

Route::get('/suppliers', 'Modules\SuppliersController@index');
Route::get('/suppliers/{supplier:skey}', 'Modules\SuppliersController@show');
Route::post('/suppliers', 'Modules\SuppliersController@store');
Route::patch('/suppliers/{supplier:skey}', 'Modules\SuppliersController@update');

Route::get('/satuan', 'Modules\SatuanController@index');
Route::get('/satuan/{satuan:id}', 'Modules\SatuanController@show');
Route::post('/satuan', 'Modules\SatuanController@store');

Route::get('/kategori', 'Modules\KategoriController@index');
Route::get('/kategori/{kategori:id}', 'Modules\KategoriController@show');
Route::post('/kategori', 'Modules\KategoriController@store');
