<?php

use Illuminate\Database\Seeder;
use App\Models\Supplier;
use Ramsey\Uuid\Uuid;
use Faker\Factory as Faker;

class SuppliersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for ($i = 0; $i < 10; $i++) {

            Supplier::insert([
                'skey' => Uuid::uuid4(),
                'name' => $faker->company,
                'email' => $faker->email,
                'address' => $faker->state,
                'contact' => $faker->phoneNumber,
                'note' => "Bpk/Ibu " . $faker->firstName,
                'created_at' => $faker->dateTime,
            ]);
        }
    }
}
