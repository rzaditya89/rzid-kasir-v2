<footer class="footer">
    <div class="container-fluid">
        <div class="copyright float-center">
            Provided By &copy;
            <script>
                document.write(new Date().getFullYear())
            </script> |
            <a href="" target="_blank">Laravel</a>
        </div>
    </div>
</footer>