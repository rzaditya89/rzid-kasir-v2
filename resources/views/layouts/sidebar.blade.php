<div class="sidebar" data-color="purple" data-background-color="white" data-image="{{ url('/assets/img/sidebar-3.jpg') }}">
    <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
    <div class="logo"><a href="" class="simple-text logo-normal">
            Kasir v.2
        </a></div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
                <a class=" nav-link" href="{{ url('/') }}">
                    <i class="material-icons">dashboard</i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/') }}">
                    <i class="material-icons">person</i>
                    <p>User Profile</p>
                </a>
            </li>
            <!-- test 2 -->
            <li class="nav-item">
                <a class="nav-link collapsed" data-toggle="collapse" href="#pagesExamples" aria-expanded="false">
                    <i class="material-icons">content_paste</i>
                    <p> Master
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="{{ Request::is('satuan') || Request::is('suppliers') || Request::is('kategori')? '' : 'collapse' }}" id="pagesExamples">
                    <ul class="nav">
                        <li class="nav-item {{ Request::is('satuan') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ url('satuan') }}">
                                <span class="sidebar-mini"> <i class="fa fa-dot-circle-o" aria-hidden="true"></i> </span>
                                <span class="sidebar-normal"> Satuan </span>
                            </a>
                        </li>
                        <li class="nav-item {{ Request::is('kategori') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ url('kategori') }}">
                                <span class="sidebar-mini"> <i class="fa fa-dot-circle-o" aria-hidden="true"></i> </span>
                                <span class="sidebar-normal"> Kategori </span>
                            </a>
                        </li>
                        <li class="nav-item {{ Request::is('suppliers') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ url('suppliers') }}">
                                <span class="sidebar-mini"> <i class="fa fa-dot-circle-o" aria-hidden="true"></i> </span>
                                <span class="sidebar-normal"> Supplier </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>