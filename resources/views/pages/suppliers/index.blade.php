@extends('layouts.master')
@section('title','Supplier')
@section('page_title','Supplier')

@section('content')
<div class="content">
  <div class="container-fluid">
    <!-- Success Notification -->
    @if (session('status'))
    <div class="alert alert-success alert-with-icon" data-notify="container">
      <i class="material-icons" data-notify="icon">library_add_check</i>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="material-icons">close</i>
      </button>
      <span data-notify="message">
        {{ session('status') }}
      </span>
    </div>
    @endif
    <div class="row">
      <div class="col-md-8">
        <div class="card card-plain">
          <div class="card-header card-header-primary">
            <div class="row">
              <div class="col-9">
                <h4 class="card-title mt-0"><i class="fa fa-clipboard mr-2"></i> Daftar Supplier Barang</h4>
              </div>
              <div class="col-3">
                <h4 class="card-title mt-0 text-right"> {{ $suppliers->total(). ' item' }}</h4>
              </div>
            </div>
            <!-- <p class="card-category"> Here is a subtitle for this table</p> -->
          </div>

          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-hover">
                <thead class="">
                  <th>
                    #
                  </th>
                  <th>
                    Nama Supplier
                  </th>
                  <th>
                    Alamat
                  </th>
                  <th class="text-center">
                    Kontak
                  </th class="text-center">
                  <th>
                    Status
                  </th>
                  <th></th>
                </thead>
                <tbody>
                  @foreach ($suppliers as $no => $data)
                  <tr>
                    <td>
                      {{$suppliers->firstItem()+$no}}
                    </td>
                    <td>
                      {{ $data->name }}
                    </td>
                    <td>
                      {{ $data->address }}
                    </td>
                    <td class="td-actions text-center">
                      {{ $data->contact }}
                    </td>
                    <td class="td-actions font-weight-bold text-center">
                      <span class="badge badge-pill badge-success">{{ $data->status }}</span>
                    </td>
                    <td class="td-actions text-left">
                      <a href="{{ url('suppliers/'. $data->skey) }}"><i class="material-icons" data-toggle="tooltip" data-html="true" title="Lihat Detail">launch</i></a>
                    </td>

                    @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title"><i class="fa fa-address-book mr-2" aria-hidden="true"></i> Supplier Baru</h4>
            <!-- <p class="card-category">Complete your profile</p> -->
          </div>
          <div class="card-body">
            <!-- Form Suppliers -->
            <form method="post" action="{{ url('suppliers') }}">
              @csrf
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="name" class="bmd-label-floating"><i class="fa fa-user mr-2" aria-hidden="true"></i> Supplier</label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror" id=" name" name="name" value="{{ old('name') }}">
                    @error('name')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="email" class="bmd-label-floating"><i class="fa fa-envelope mr-2" aria-hidden="true"></i> Email</label>
                    <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email') }}">

                    @error('email')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="address" class="bmd-label-floating"><i class="fa fa-map-marker mr-2" aria-hidden="true"></i> Alamat</label>
                    <input type="text" class="form-control @error('address') is-invalid @enderror" id="address" name="address" value="{{ old('address') }}">
                    @error('address')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="contact" class="bmd-label-floating"><i class="fa fa-phone mr-2" aria-hidden="true"></i> Kontak</label>
                    <input type="text" class="form-control @error('contact') is-invalid @enderror" id="contact" name="contact" value="{{ old('contact') }}">
                    @error('contact')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="note"><i class="fa fa-commenting mr-2" aria-hidden="true"></i> Catatan</label>
                    <div class="form-group">
                      <label class="bmd-label-floating"><i>Silahkan menambahkan catatan (opsional)</i></label>
                      <textarea class="form-control" rows="3" id="note" name="note"></textarea>
                    </div>
                  </div>
                </div>
              </div>

              <input class="btn btn-primary" type="reset" value="Reset">
              <button type="submit" class="btn btn-primary pull-right">Tambah Supplier</button>

              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>
      {{ $suppliers->links() }}
    </div>
  </div>
</div>

@endsection

@push('page-scripts')

@endpush