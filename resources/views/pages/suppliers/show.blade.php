@extends('layouts.master')
@section('title',' Detail Supplier')
@section('page_title','Detail Data Supplier')

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8">
        <div class="card card-nav-tabs">
          <div class="card-header card-header-warning">
            <div class="row">
              <div class="col">
                <h4 class="card-title font-weight-normal"><i class="fa fa-address-card mr-2"></i> Detail Supplier : </h4>
              </div>
              <div class="col">
                <h4 class="font-weight-normal text-right">{{ $supplier->name }}</h4>
              </div>
            </div>
          </div>
          <div class="card-body">
            <form>
              <div class="row">
                <div class="col-sm-6">
                  <div class="table-responsive">
                    <table class="table table-sm table-hover table-striped">
                      <tbody>
                        <tr>
                          <td>Nama </td>
                          <td class="text-right font-weight-bold"> {{ $supplier->name }}</td>
                        </tr>
                        <tr>
                          <td>Alamat </td>
                          <td class="text-right font-weight-bold"> {{ $supplier->address }}</td>
                        </tr>
                        <tr>
                          <td>Kontak </td>
                          <td class="text-right font-weight-bold"> {{ $supplier->contact }}</td>
                        </tr>
                        <tr>
                          <td>Email </td>
                          <td class="text-right font-weight-bold"> {{ $supplier->email }}</td>
                        </tr>
                        <tr>
                          <td>Dibuat pada</td>
                          <td class="text-right font-weight-bold"> {{ $supplier->created_at->format('d-m-Y') }}</td>
                        </tr>
                        <tr>
                          <td>Waktu</td>
                          <td class="text-right font-weight-bold">{{ $supplier->created_at->format('H:i:s') }}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="table-responsive">
                    <table class="table table-sm table-hover table-striped">
                      <tbody>
                        <tr>
                          <td>Dibuat oleh</td>
                          <td class="text-right font-weight-bold"></td>
                        </tr>
                        <tr>
                          <td>Status</td>
                          <td class="text-right font-weight-bold"></td>
                        </tr>
                        <tr>
                          <td>Catatan</td>
                          <td class="text-right font-weight-bold">{{ $supplier->note }}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </form>
            <a href="{{ url('suppliers') }}" class="btn btn-default btn-sm"><i class="material-icons">list</i> Kembali Ke Daftar Supplier</a>
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#supplierEditModal">
              <i class="material-icons">edit</i> Ubah Data Supplier
            </button>

            <!-- Modal -->
            <form method="post" action="{{ $supplier->skey }}">
              @method('patch')
              @csrf
              <div class="modal fade" id="supplierEditModal" tabindex="-1" role="dialog" aria-labelledby="supplierEditModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title text-center" id="supplierEditModalLabel">Ubah Data Supplier</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="name" class="bmd-label-floating">Supplier</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{ $supplier->name }}" required>
                            @error('name')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="email" class="bmd-label-floating">Email</label>
                            <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{{ $supplier->email }}" required>
                            @error('email')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="address" class="bmd-label-floating">Alamat</label>
                            <input type="text" class="form-control @error('address') is-invalid @enderror" id="address" name="address" value="{{ $supplier->address }}" required>
                            @error('address')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="contact" class="bmd-label-floating">Kontak</label>
                            <input type="text" class="form-control @error('contact') is-invalid @enderror" id="contact" name="contact" value="{{ $supplier->contact }}" required>
                            @error('contact')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="note">Catatan</label>
                            <div class="form-group">
                              <label class="bmd-label-floating"><i>Ubah catatan (opsional)</i></label>
                              <textarea class="form-control" rows="3" id="note" name="note">{{ $supplier->note }}</textarea>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
            <!-- End Modal -->
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="col-md-4">
        <div class="card card-profile ml-auto mr-auto" style="max-width: 360px">
          <div class="card-header card-header-image">
            <a href="">
              <img class="img" src="{{ asset('assets/img/faces/supplier_bg.jpg') }}" rel="nofollow">
            </a>
          </div>

          <div class="card-body ">
            <h4 class="card-title">{{ $supplier->name }}</h4>
            <h6 class="card-category text-gray">{{ $supplier->email }}</h6>
          </div>
          <div class="card-footer justify-content-center">
            <a href="#pablo" class="btn btn-just-icon btn-google btn-round">
              <i class="fa fa-google"></i>
            </a>
            <a href="#pablo" class="btn btn-just-icon btn-facebook btn-round">
              <i class="fa fa-facebook-square"></i>
            </a>
            <a href="#pablo" class="btn btn-just-icon btn-youtube btn-round">
              <i class="fa fa-youtube"></i>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@push('page-scripts')

@endpush