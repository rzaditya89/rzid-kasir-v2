@extends('layouts.master')
@section('title',' Detail Satuan Barang')
@section('page_title','Detail Satuan Barang')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card card-nav-tabs">
                    <div class="card-header card-header-info">
                        <div class="row">
                            <div class="col">
                                <h4 class="card-title font-weight-normal"><i class="fa fa-address-card mr-2"></i> Detail Satuan : </h4>
                            </div>
                            <div class="col">
                                <h4 class="font-weight-normal text-right">{{ $satuan->nama }}</h4>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <table class="table table-sm table-hover table-striped">
                                            <tbody>
                                                <tr>
                                                    <td>Nama </td>
                                                    <td class="text-right font-weight-bold"> {{ $satuan->nama }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Kode </td>
                                                    <td class="text-right font-weight-bold"> {{ $satuan->kode }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Induk Satuan </td>
                                                    <td class="text-right font-weight-bold"> - </td>
                                                </tr>
                                                <tr>
                                                    <td>Rasio </td>
                                                    <td class="text-right font-weight-bold"> 1 : {{ $satuan->rasio }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Dibuat pada</td>
                                                    <td class="text-right font-weight-bold"> {{ $satuan->created_at->format('d-m-Y') }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Waktu</td>
                                                    <td class="text-right font-weight-bold">{{ $satuan->created_at->format('H:i:s') }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <a href="{{ url('satuan') }}" class="btn btn-default btn-sm"><i class="material-icons">list</i> Kembali Ke Daftar Satuan</a>
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#supplierEditModal">
                            <i class="material-icons">edit</i> Ubah Data Satuan
                        </button>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('page-scripts')

@endpush