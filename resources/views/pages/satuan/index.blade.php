@extends('layouts.master')
@section('title','Satuan')
@section('page_title','Satuan')

@section('content')
<div class="content">
    <div class="container-fluid">
        <!-- Success Notification -->
        @if (session('status'))
        <div class="alert alert-success alert-with-icon" data-notify="container">
            <i class="material-icons" data-notify="icon">library_add_check</i>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <i class="material-icons">close</i>
            </button>
            <span data-notify="message">
                {{ session('status') }}
            </span>
        </div>
        @endif
        <div class="row">
            <div class="col-md-7">
                <div class="card card-plain">
                    <div class="card-header card-header-primary">
                        <div class="row">
                            <div class="col-9">
                                <h4 class="card-title mt-0"><i class="fa fa-clipboard mr-2"></i> Daftar Satuan Barang</h4>
                            </div>
                            <div class="col-3">
                                <h4 class="card-title mt-0 text-right"> {{ $satuan->total(). ' item' }}</h4>
                            </div>
                        </div>
                        <!-- <p class="card-category"> Here is a subtitle for this table</p> -->
                    </div>

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead class="">
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Satuan
                                    </th>
                                    <th>
                                        Kode
                                    </th>
                                    <th class="text-center">
                                        Induk Satuan
                                    </th>
                                    <th class="text-center">
                                        Rasio
                                    </th>
                                    <th></th>
                                </thead>
                                <tbody>
                                    @foreach ($satuan as $no => $data)
                                    <tr>
                                        <td>
                                            {{$satuan->firstItem()+$no}}
                                        </td>
                                        <td>
                                            {{ $data->nama }}
                                        </td>
                                        <td>
                                            {{ $data->kode }}
                                        </td>
                                        <td class="td-actions text-center">-
                                            <!-- {{ $data->contact }} -->
                                        </td>
                                        <td class="td-actions font-weight-bold text-center">
                                            <span class="badge badge-pill badge-success">1 : {{ $data->rasio }}</span>
                                        </td>
                                        <td class="td-actions text-left">
                                            <a href="{{ url('satuan/'. $data->id) }}"><i class="material-icons" data-toggle="tooltip" data-html="true" title="Lihat Detail">launch</i></a>
                                        </td>

                                        @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title"><i class="fa fa-file-o mr-2" aria-hidden="true"></i> Satuan Baru</h4>
                        <!-- <p class="card-category">Complete your profile</p> -->
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ url('satuan') }}" class="form-horizontal">
                            @csrf
                            <div class="row my-3">
                                <label for="nama" class="col-md-3 col-form-label font-weight-bold"><i class="fa fa-edit mr-2" aria-hidden="true"></i> Satuan</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" name="nama" value="{{ old('nama') }}" placeholder="contoh: gram">
                                    @error('nama')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row my-3">
                                <label for="kode" class="col-md-3 col-form-label font-weight-bold"><i class="fa fa-tag mr-2" aria-hidden="true"></i> Kode</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control @error('kode') is-invalid @enderror" id="kode" name="kode" value="{{ old('kode') }}" placeholder="contoh: gr">
                                    @error('kode')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row my-3">
                                <label for="induk" class="col-md-4 col-form-label font-weight-bold"><i class="fa fa-info mr-2" aria-hidden="true"></i> Induk Satuan</label>
                                <div class="col-md-8">
                                    <select class="custom-select" data-style="btn btn-link" id="induk" name="induk">
                                        <option selected disabled>Pilih induk...</option>
                                        <option class="hidden" value="">Tidak Ada Induk</option>
                                        @foreach ($satuan as $data2)
                                        <option>{{ $data2->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <label for="rasio" class="col-md-3 col-form-label font-weight-bold"><i class="fa fa-window-restore mr-2" aria-hidden="true"></i> Rasio</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control @error('rasio') is-invalid @enderror" id="rasio" name="rasio" value="{{ old('rasio') }}" placeholder="contoh: 12, artinya 1 induk untuk 12 anak">
                                    @error('rasio')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mt-5 border-top">
                                <div class="col-6 mt-2">
                                    <input class="btn btn-primary" type="reset" value="Reset">
                                </div>
                                <div class="col-6 mt-2">
                                    <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
            {{ $satuan->links() }}
        </div>
    </div>
</div>

@endsection

@push('page-scripts')

@endpush