@extends('layouts.master')
@section('title',' Detail Kategori')
@section('page_title','Detail Kategori')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card card-nav-tabs">
                    <div class="card-header card-header-info">
                        <div class="row">
                            <div class="col">
                                <h4 class="card-title font-weight-normal"><i class="fa fa-address-card mr-2"></i> Detail Kategori : </h4>
                            </div>
                            <div class="col">
                                <h4 class="font-weight-normal text-right">{{ $kategori->name }}</h4>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <table class="table table-sm table-hover table-striped">
                                            <tbody>
                                                <tr>
                                                    <td>ID </td>
                                                    <td class="text-right font-weight-bold"> {{ $kategori->id }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Kategori </td>
                                                    <td class="text-right font-weight-bold"> {{ $kategori->name }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Catatan </td>
                                                    <td class="text-right text-danger font-weight-bold"> {{ $kategori->note }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Dibuat pada</td>
                                                    <td class="text-right font-weight-bold"> {{ $kategori->created_at}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Oleh</td>
                                                    <td class="text-right font-weight-bold">{{ $kategori->CreateBy->name }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <a href="{{ url('kategori') }}" class="btn btn-default btn-sm"><i class="material-icons">list</i> Kembali Ke Daftar Kategori</a>
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#supplierEditModal">
                            <i class="material-icons">edit</i> Ubah Data Kategori
                        </button>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('page-scripts')

@endpush