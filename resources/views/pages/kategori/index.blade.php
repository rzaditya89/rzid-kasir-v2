@extends('layouts.master')
@section('title','Kategori')
@section('page_title','Kategori')

@section('content')
<div class="content">
  <div class="container-fluid">
    <!-- Success Notification -->
    @if (session('status'))
    <div class="alert alert-success alert-with-icon" data-notify="container">
      <i class="material-icons" data-notify="icon">library_add_check</i>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="material-icons">close</i>
      </button>
      <span data-notify="message">
        {{ session('status') }}
      </span>
    </div>
    @endif
    <div class="row">
      <div class="col-md-6">
        <div class="card card-plain">
          <div class="card-header card-header-primary">
            <div class="row">
              <div class="col-9">
                <h4 class="card-title mt-0"><i class="fa fa-clipboard mr-2"></i> Daftar Kategori Barang</h4>
              </div>
              <div class="col-3">
                <h4 class="card-title mt-0 text-right"> {{ $kategori->total(). ' item' }}</h4>
              </div>
            </div>
            <!-- <p class="card-category"> Here is a subtitle for this table</p> -->
          </div>

          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-hover ">
                <thead class="">
                  <th>
                    #
                  </th>
                  <th>
                    Nama Kategori
                  </th>
                  <th class="text-center">
                    Dibuat pada
                  </th>
                  <th class="text-center">
                    Dibuat oleh
                  </th>
                </thead>
                <tbody class="mt-5">
                  @foreach ($kategori as $no => $data)
                  <tr>
                    <td>
                      {{$kategori->firstItem()+$no}}
                    </td>
                    <td>
                      <a href="{{ url('kategori/'. $data->id) }}" class="btn btn-info btn-round btn-sm" title="Lihat Detail">
                        {{ $data->name }}
                        <div class="ripple-container"></div>
                      </a>
                    </td>
                    <td class="td-actions text-center">
                      {{ $data->created_at->format('d-m-Y') }}
                    </td>
                    <td class="td-actions text-center">
                      {{ $data->CreateBy->name }}
                    </td>
                    <!-- <td class="td-actions text-left">
                                            <a href="{{ url('satuan/'. $data->id) }}"><i class="material-icons" data-toggle="tooltip" data-html="true" title="Lihat Detail">launch</i></a>
                                        </td> -->

                    @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title"><i class="fa fa-file-o mr-2" aria-hidden="true"></i> Kategori Baru</h4>
            <!-- <p class="card-category">Complete your profile</p> -->
          </div>
          <div class="card-body">
            <form method="post" action="{{ url('kategori') }}" class="form-horizontal">
              @csrf
              <div class="row my-3">
                <label for="name" class="col-md-4 col-form-label font-weight-bold"><i class="fa fa-edit mr-2" aria-hidden="true"></i> Nama Kategori</label>
                <div class="col-md-8">
                  <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{ old('name') }}" placeholder="contoh: baju anak">
                  @error('name')
                  <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <div class="row my-3">
                <label for="note" class="col-md-3 col-form-label font-weight-bold"><i class="fa fa-commenting mr-2" aria-hidden="true"></i> Catatan</label>
                <div class="col-md-9 my-2">
                  <label class="bmd-label-floating"><i>Silahkan menambahkan keterangan (opsional)</i></label>
                  <textarea class="form-control" rows="3" id="note" name="note"></textarea>
                </div>
              </div>
              <div class="row mt-3 border-top">
                <div class="col-6 mt-2">
                  <input class="btn btn-primary" type="reset" value="Reset">
                </div>
                <div class="col-6 mt-2">
                  <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                </div>
              </div>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
      </div>
      {{ $kategori->links() }}
    </div>
  </div>
</div>

@endsection

@push('page-scripts')

@endpush